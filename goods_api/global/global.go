package global

import (
	"api/goods"
	"github.com/smartwalle/alipay/v3"
)

var (
	Good   goods.GoodsClient
	Client *alipay.Client
)
