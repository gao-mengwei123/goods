package api

import (
	"api/global"
	pb "api/goods"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/smartwalle/alipay/v3"
	"log"
	"net/http"
)

func Create(c *gin.Context) {

	type good struct {
		GoodsName  string  `json:"goods_name"`
		GoodsPrice float64 `json:"goods_price"`
		GoodsCount int64   `json:"goods_count"`
		GoodsTest  string  `json:"goods_test"`
	}
	var g good
	if err := c.BindJSON(&g); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "数据接受失败",
		})
	}

	r, err := global.Good.CreateGoods(c, &pb.GoodsRequest{
		GoodsName:  g.GoodsName,
		GoodsPrice: float32(g.GoodsPrice),
		GoodsCount: g.GoodsCount,
		GoodsTest:  g.GoodsTest,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "商品添加失败",
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"message": r,
	})
}

func CrtGoods(c *gin.Context) {

	type good struct {
		GoodsID     int64 `json:"goods_id"`
		GoodsNumber int64 `json:"goods_number"`
		GoodsYou    int64 `json:"goods_you"`
	}
	var g good
	if err := c.BindJSON(&g); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "数据接受失败",
		})
	}

	goods, err := global.Good.CrtGoods(c, &pb.CrtRequest{
		GoodsId:     g.GoodsID,
		GoodsNumber: g.GoodsNumber,
		GoodsYou:    g.GoodsYou,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "订单生成失败",
		})
	}

	var p = alipay.TradePagePay{}
	p.NotifyURL = "http://499b42ec.r20.cpolar.top" + "/alipay/notify"
	p.ReturnURL = "http://499b42ec.r20.cpolar.top" + "/alipay/callback"
	p.Subject = "支付测试:"
	p.OutTradeNo = goods.OrderUid
	p.TotalAmount = goods.GoodsPrice
	p.ProductCode = "FAST_INSTANT_TRADE_PAY"

	url, _ := global.Client.TradePagePay(p)

	c.JSON(http.StatusOK, gin.H{
		"message": "订单生成成功",
		"data":    url.String(),
	})

}

func Callback(c *gin.Context) {

	c.Request.ParseForm()

	if err := global.Client.VerifySign(c.Request.Form); err != nil {
		log.Println("回调验证签名发生错误", err)
		c.Writer.WriteHeader(http.StatusBadRequest)
		c.Writer.Write([]byte("回调验证签名发生错误"))
		return
	}

	log.Println("回调验证签名通过")

	// 示例一：使用已有接口进行查询
	var outTradeNo = c.Request.Form.Get("out_trade_no")
	var p = alipay.TradeQuery{}
	p.OutTradeNo = outTradeNo

	rsp, err := global.Client.TradeQuery(p)
	if err != nil {
		c.Writer.WriteHeader(http.StatusBadRequest)
		c.Writer.Write([]byte(fmt.Sprintf("验证订单 %s 信息发生错误: %s", outTradeNo, err.Error())))
		return
	}

	if rsp.IsFailure() {
		c.Writer.WriteHeader(http.StatusBadRequest)
		c.Writer.Write([]byte(fmt.Sprintf("验证订单 %s 信息发生错误: %s-%s", outTradeNo, rsp.Msg, rsp.SubMsg)))
		return
	}
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write([]byte(fmt.Sprintf("订单 %s 支付成功", outTradeNo)))

}

func Notify(c *gin.Context) {

	c.Request.ParseForm()

	var notification, err = global.Client.DecodeNotification(c.Request.Form)
	if err != nil {
		log.Println("解析异步通知发生错误", err)
		return
	}

	log.Println("解析异步通知成功:", notification.NotifyId)

	// 示例一：使用自定义请求进行查询
	var p = alipay.NewPayload("alipay.trade.query")
	p.AddBizField("out_trade_no", notification.OutTradeNo)

	var rsp *alipay.TradeQueryRsp
	if err = global.Client.Request(p, &rsp); err != nil {
		log.Printf("异步通知验证订单 %s 信息发生错误: %s \n", notification.OutTradeNo, err.Error())
		return
	}
	if rsp.IsFailure() {
		log.Printf("异步通知验证订单 %s 信息发生错误: %s-%s \n", notification.OutTradeNo, rsp.Msg, rsp.SubMsg)
		return
	}

	log.Printf("订单 %s 支付成功 \n", notification.OutTradeNo)

	global.Client.ACKNotification(c.Writer)

}
