package api

import (
	"github.com/gin-gonic/gin"
)

func Register(r *gin.Engine) {

	group := r.Group("goods")
	{
		group.POST("create", Create)
		group.POST("crtGoods", CrtGoods)

	}
	//支付宝sdk
	r.GET("/alipay/callback", Callback)
	r.POST("/alipay/notify", Notify)

}
