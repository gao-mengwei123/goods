package main

import (
	"api/api"
	_ "api/initaize"
	"github.com/gin-gonic/gin"
)

func main() {

	r := gin.Default()
	api.Register(r)
	r.Run(":9000") // 监听并在 0.0.0.0:8080 上启动服务

}
