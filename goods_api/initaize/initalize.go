package initaize

import (
	"api/global"
	pb "api/goods"
	"github.com/smartwalle/alipay/v3"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

const (
	kAppId      = "9021000128664143"
	kPrivateKey = "MIIEowIBAAKCAQEAi1FxqDMjQpQY4EJ4sNqQhRX7/WsJc596QNsDMxBeN5mVK4epb4EfSCwF+7ldhGpeqHJa2kl2jcSfSBtEFkXhwsc6FcBLj7GCungxq9aGh1Ic6sQE4LAKe8UhdeeLbr9yS0ZZy23q4i0K8Ro4+PKZmLzWYUh1ye+bqbTEPtTPQTsVv71WAKtHicxLlSII5LX3WQ6KxkxBkPMBWBlhDT7JUBKsXhi1AuAkKzD3cE5TJiGnhsDWgacxBmXg4MiCZlFEcUV/o602otvPTL1LRuzbsej+Nkzni8zud97GcR3qjHiRO0I7t5nEDYe9msqmfXuHA4IttvYzyPYsbnuyrR3MOQIDAQABAoIBAGnsjmG4H8LrHs7DO0gbpW3KfUD8NL94LVdKLXCRrVDQsJ3cE3L31XTmI9Zz5SpcRYQFDhaRbFX1fLUyJd5ZW4DgQMVw4wJpkF9+KyXfgYE5aUsFpj+/GPlptDsUxOp7HMcmwfbtp3w3iu4tZcvBiA6c5NIqaYQTbWLmUZ8D5mBND6ExnMNzDUGyKPZHTjF2oafdl29BLJDr1g5HuTaTT+HliaM6/ZgsDllDL1rtk7SheQ5ggb9rxk8DbR+wM08tQx9XBnEG8KGuNOQ6vTXk+1nPbldohRXaZQPe9LKYiohzZJ54ISzenFSqfAlcNSSTrtVKCeofs6Clf5PdlBbsGhECgYEAzyJ5lZSpXGSu7U8pwOmklEDnMJstRK4izek/9+64imuiGrVWHi/7TDtAylI3Q+TkMhOQNXOFXaZqJeLgzopHrTbgtcpOPjPl1797259TFuG75W391ZlpfvlpmjQgxyyTs1LTXLGfxwtNpKBl/OOUPPE4XxE+9sO3/hT2Q7dfTMsCgYEArC9P4xXhZ1l7G1umQsxKUSlL/2mHxtjyZwukVK4Wvtr1Y/iYqVgi2kCxzTzZhGxue+yYbhcAkNaIB4GwnH8f9Wipr5CC/L7zOHV6+H+eSJcnfFYDAms6qzPhH2gZCkwyUSBJOWEufTprK0TeH/LXtwa2uuBw/LHsskc0YbkEDosCgYEAvDjGpT8muxeewXQ7K9rkO/fSXZ/U/eEDLXu0LjcWKGpwLrbzX5EYWQhK8s7M+3wmRL70Py4PVa9gK7dDs0uCoIsKnQcOHIYKaQfOpB7eYR+VH4HYtF2ouMIpzP9rfbIrMTulZ0jdOQEzd9JteuTOkqZcJD9GqPPm4RLpSVV7WwsCgYB3m4H9+BQU68alkXukCS9Sw8IkEVWdHtT8Gc00eZ4ZRBaUNHsvn6bOP4SDUlKzN3NkKE0WpGnjMvNZa5CYlYJIwkSlTViAY9Cmlo0uk9ALEtz99O2wP3aIuMgYPgKu8ZE+Y5QaBNT7bppFe1hljxaj8Ej89a3nC12l1eqgO5qC0wKBgD1xIJXOueCWdHbz84vByiIB1qwzwkQ9HrFoQI2tmJIwdWFakYz7kIVjI9T+vNcdzr9RNkr9E2bRHqd4FT35lhCmxTKIuTXGjknyV5fELrss158LY4tFLui5FNZ140wEehzV/YizKOerPZAvKLpGZnRULDxuZtpa+9lkECHqY1S3"
	kServerPort = "9000"
	// TODO 设置回调地址域名
	kServerDomain = "http://499b42ec.r20.cpolar.top"
)

func init() {
	Consul()
	zfb()
}

func Consul() {

	conn, err := grpc.Dial("127.0.0.1:8081", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	c := pb.NewGoodsClient(conn)
	global.Good = c
}

func zfb() {

	client, err := alipay.New(kAppId, kPrivateKey, false)
	if err != nil {
		log.Println("初始化支付宝失败", err)
		return
	}
	global.Client = client
	// 加载证书
	if err := client.LoadAliPayPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAj/kmC5o4lmp3aGp1xUtFFdg27l6FZZEH8Hs7yiz7W94QNJouoJxvzHNupq3KNJGZj9f6t6dfxOlyxW50zgjL0HiYPgz9FvId0eqfAnh+fe1fsvokwdni0AChiT3+bONwBa0eBmtWGd8JixZhZsnquG2dW0UfdnXpl/2uR77jfHSDgK38oQOsVniPx+VwdfRjXJPA5coW+6hYuzrZLqizUeH1yQDxvkCRVbopVftCRHFe32jfHIGnAjYy8PCkC6MmL9iRU1t7+0oLSzLp3S6xjdMQSiJOSDxFgN38QGs3ZKM8fRvTEKVcNAGgFzcjlCefrKZlwd9MD3iWxX2dL4RTxQIDAQAB"); err != nil {
		log.Println("加载公钥失败", err)
		return
	}

}
