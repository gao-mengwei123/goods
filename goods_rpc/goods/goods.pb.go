// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.33.0
// 	protoc        v3.19.4
// source: goods.proto

package goods

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type GoodsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	GoodsName  string  `protobuf:"bytes,10,opt,name=GoodsName,proto3" json:"GoodsName,omitempty"`
	GoodsPrice float32 `protobuf:"fixed32,20,opt,name=GoodsPrice,proto3" json:"GoodsPrice,omitempty"`
	GoodsCount int64   `protobuf:"varint,30,opt,name=GoodsCount,proto3" json:"GoodsCount,omitempty"`
	GoodsTest  string  `protobuf:"bytes,40,opt,name=GoodsTest,proto3" json:"GoodsTest,omitempty"`
}

func (x *GoodsRequest) Reset() {
	*x = GoodsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_goods_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GoodsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GoodsRequest) ProtoMessage() {}

func (x *GoodsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_goods_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GoodsRequest.ProtoReflect.Descriptor instead.
func (*GoodsRequest) Descriptor() ([]byte, []int) {
	return file_goods_proto_rawDescGZIP(), []int{0}
}

func (x *GoodsRequest) GetGoodsName() string {
	if x != nil {
		return x.GoodsName
	}
	return ""
}

func (x *GoodsRequest) GetGoodsPrice() float32 {
	if x != nil {
		return x.GoodsPrice
	}
	return 0
}

func (x *GoodsRequest) GetGoodsCount() int64 {
	if x != nil {
		return x.GoodsCount
	}
	return 0
}

func (x *GoodsRequest) GetGoodsTest() string {
	if x != nil {
		return x.GoodsTest
	}
	return ""
}

type Good struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         int64   `protobuf:"varint,5,opt,name=Id,proto3" json:"Id,omitempty"`
	GoodsName  string  `protobuf:"bytes,10,opt,name=GoodsName,proto3" json:"GoodsName,omitempty"`
	GoodsPrice float32 `protobuf:"fixed32,20,opt,name=GoodsPrice,proto3" json:"GoodsPrice,omitempty"`
	GoodsCount int64   `protobuf:"varint,30,opt,name=GoodsCount,proto3" json:"GoodsCount,omitempty"`
	GoodsTest  string  `protobuf:"bytes,40,opt,name=GoodsTest,proto3" json:"GoodsTest,omitempty"`
}

func (x *Good) Reset() {
	*x = Good{}
	if protoimpl.UnsafeEnabled {
		mi := &file_goods_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Good) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Good) ProtoMessage() {}

func (x *Good) ProtoReflect() protoreflect.Message {
	mi := &file_goods_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Good.ProtoReflect.Descriptor instead.
func (*Good) Descriptor() ([]byte, []int) {
	return file_goods_proto_rawDescGZIP(), []int{1}
}

func (x *Good) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *Good) GetGoodsName() string {
	if x != nil {
		return x.GoodsName
	}
	return ""
}

func (x *Good) GetGoodsPrice() float32 {
	if x != nil {
		return x.GoodsPrice
	}
	return 0
}

func (x *Good) GetGoodsCount() int64 {
	if x != nil {
		return x.GoodsCount
	}
	return 0
}

func (x *Good) GetGoodsTest() string {
	if x != nil {
		return x.GoodsTest
	}
	return ""
}

type GoodsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	G *Good `protobuf:"bytes,10,opt,name=G,proto3" json:"G,omitempty"`
}

func (x *GoodsResponse) Reset() {
	*x = GoodsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_goods_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GoodsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GoodsResponse) ProtoMessage() {}

func (x *GoodsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_goods_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GoodsResponse.ProtoReflect.Descriptor instead.
func (*GoodsResponse) Descriptor() ([]byte, []int) {
	return file_goods_proto_rawDescGZIP(), []int{2}
}

func (x *GoodsResponse) GetG() *Good {
	if x != nil {
		return x.G
	}
	return nil
}

type CrtRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	GoodsId     int64 `protobuf:"varint,10,opt,name=GoodsId,proto3" json:"GoodsId,omitempty"`
	GoodsNumber int64 `protobuf:"varint,20,opt,name=GoodsNumber,proto3" json:"GoodsNumber,omitempty"`
	GoodsYou    int64 `protobuf:"varint,30,opt,name=GoodsYou,proto3" json:"GoodsYou,omitempty"`
}

func (x *CrtRequest) Reset() {
	*x = CrtRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_goods_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CrtRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CrtRequest) ProtoMessage() {}

func (x *CrtRequest) ProtoReflect() protoreflect.Message {
	mi := &file_goods_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CrtRequest.ProtoReflect.Descriptor instead.
func (*CrtRequest) Descriptor() ([]byte, []int) {
	return file_goods_proto_rawDescGZIP(), []int{3}
}

func (x *CrtRequest) GetGoodsId() int64 {
	if x != nil {
		return x.GoodsId
	}
	return 0
}

func (x *CrtRequest) GetGoodsNumber() int64 {
	if x != nil {
		return x.GoodsNumber
	}
	return 0
}

func (x *CrtRequest) GetGoodsYou() int64 {
	if x != nil {
		return x.GoodsYou
	}
	return 0
}

type CrtResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	OrderUid   string `protobuf:"bytes,10,opt,name=OrderUid,proto3" json:"OrderUid,omitempty"`
	GoodsPrice string `protobuf:"bytes,20,opt,name=GoodsPrice,proto3" json:"GoodsPrice,omitempty"`
}

func (x *CrtResponse) Reset() {
	*x = CrtResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_goods_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CrtResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CrtResponse) ProtoMessage() {}

func (x *CrtResponse) ProtoReflect() protoreflect.Message {
	mi := &file_goods_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CrtResponse.ProtoReflect.Descriptor instead.
func (*CrtResponse) Descriptor() ([]byte, []int) {
	return file_goods_proto_rawDescGZIP(), []int{4}
}

func (x *CrtResponse) GetOrderUid() string {
	if x != nil {
		return x.OrderUid
	}
	return ""
}

func (x *CrtResponse) GetGoodsPrice() string {
	if x != nil {
		return x.GoodsPrice
	}
	return ""
}

var File_goods_proto protoreflect.FileDescriptor

var file_goods_proto_rawDesc = []byte{
	0x0a, 0x0b, 0x67, 0x6f, 0x6f, 0x64, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x05, 0x67,
	0x6f, 0x6f, 0x64, 0x73, 0x22, 0x8a, 0x01, 0x0a, 0x0c, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1c, 0x0a, 0x09, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x4e, 0x61,
	0x6d, 0x65, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x4e,
	0x61, 0x6d, 0x65, 0x12, 0x1e, 0x0a, 0x0a, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x50, 0x72, 0x69, 0x63,
	0x65, 0x18, 0x14, 0x20, 0x01, 0x28, 0x02, 0x52, 0x0a, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x50, 0x72,
	0x69, 0x63, 0x65, 0x12, 0x1e, 0x0a, 0x0a, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x43, 0x6f, 0x75, 0x6e,
	0x74, 0x18, 0x1e, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0a, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x43, 0x6f,
	0x75, 0x6e, 0x74, 0x12, 0x1c, 0x0a, 0x09, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x54, 0x65, 0x73, 0x74,
	0x18, 0x28, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x54, 0x65, 0x73,
	0x74, 0x22, 0x92, 0x01, 0x0a, 0x04, 0x47, 0x6f, 0x6f, 0x64, 0x12, 0x0e, 0x0a, 0x02, 0x49, 0x64,
	0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x49, 0x64, 0x12, 0x1c, 0x0a, 0x09, 0x47, 0x6f,
	0x6f, 0x64, 0x73, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x47,
	0x6f, 0x6f, 0x64, 0x73, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x1e, 0x0a, 0x0a, 0x47, 0x6f, 0x6f, 0x64,
	0x73, 0x50, 0x72, 0x69, 0x63, 0x65, 0x18, 0x14, 0x20, 0x01, 0x28, 0x02, 0x52, 0x0a, 0x47, 0x6f,
	0x6f, 0x64, 0x73, 0x50, 0x72, 0x69, 0x63, 0x65, 0x12, 0x1e, 0x0a, 0x0a, 0x47, 0x6f, 0x6f, 0x64,
	0x73, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x1e, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0a, 0x47, 0x6f,
	0x6f, 0x64, 0x73, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x1c, 0x0a, 0x09, 0x47, 0x6f, 0x6f, 0x64,
	0x73, 0x54, 0x65, 0x73, 0x74, 0x18, 0x28, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x47, 0x6f, 0x6f,
	0x64, 0x73, 0x54, 0x65, 0x73, 0x74, 0x22, 0x2a, 0x0a, 0x0d, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x19, 0x0a, 0x01, 0x47, 0x18, 0x0a, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x0b, 0x2e, 0x67, 0x6f, 0x6f, 0x64, 0x73, 0x2e, 0x47, 0x6f, 0x6f, 0x64, 0x52,
	0x01, 0x47, 0x22, 0x64, 0x0a, 0x0a, 0x43, 0x72, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x18, 0x0a, 0x07, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x49, 0x64, 0x18, 0x0a, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x07, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x49, 0x64, 0x12, 0x20, 0x0a, 0x0b, 0x47, 0x6f,
	0x6f, 0x64, 0x73, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x14, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x0b, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x12, 0x1a, 0x0a, 0x08,
	0x47, 0x6f, 0x6f, 0x64, 0x73, 0x59, 0x6f, 0x75, 0x18, 0x1e, 0x20, 0x01, 0x28, 0x03, 0x52, 0x08,
	0x47, 0x6f, 0x6f, 0x64, 0x73, 0x59, 0x6f, 0x75, 0x22, 0x49, 0x0a, 0x0b, 0x43, 0x72, 0x74, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x4f, 0x72, 0x64, 0x65, 0x72,
	0x55, 0x69, 0x64, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x4f, 0x72, 0x64, 0x65, 0x72,
	0x55, 0x69, 0x64, 0x12, 0x1e, 0x0a, 0x0a, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x50, 0x72, 0x69, 0x63,
	0x65, 0x18, 0x14, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x50, 0x72,
	0x69, 0x63, 0x65, 0x32, 0x74, 0x0a, 0x05, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x12, 0x38, 0x0a, 0x0b,
	0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x12, 0x13, 0x2e, 0x67, 0x6f,
	0x6f, 0x64, 0x73, 0x2e, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x14, 0x2e, 0x67, 0x6f, 0x6f, 0x64, 0x73, 0x2e, 0x47, 0x6f, 0x6f, 0x64, 0x73, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x31, 0x0a, 0x08, 0x43, 0x72, 0x74, 0x47, 0x6f, 0x6f,
	0x64, 0x73, 0x12, 0x11, 0x2e, 0x67, 0x6f, 0x6f, 0x64, 0x73, 0x2e, 0x43, 0x72, 0x74, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x12, 0x2e, 0x67, 0x6f, 0x6f, 0x64, 0x73, 0x2e, 0x43, 0x72,
	0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x09, 0x5a, 0x07, 0x2e, 0x2f, 0x67,
	0x6f, 0x6f, 0x64, 0x73, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_goods_proto_rawDescOnce sync.Once
	file_goods_proto_rawDescData = file_goods_proto_rawDesc
)

func file_goods_proto_rawDescGZIP() []byte {
	file_goods_proto_rawDescOnce.Do(func() {
		file_goods_proto_rawDescData = protoimpl.X.CompressGZIP(file_goods_proto_rawDescData)
	})
	return file_goods_proto_rawDescData
}

var file_goods_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_goods_proto_goTypes = []interface{}{
	(*GoodsRequest)(nil),  // 0: goods.GoodsRequest
	(*Good)(nil),          // 1: goods.Good
	(*GoodsResponse)(nil), // 2: goods.GoodsResponse
	(*CrtRequest)(nil),    // 3: goods.CrtRequest
	(*CrtResponse)(nil),   // 4: goods.CrtResponse
}
var file_goods_proto_depIdxs = []int32{
	1, // 0: goods.GoodsResponse.G:type_name -> goods.Good
	0, // 1: goods.Goods.CreateGoods:input_type -> goods.GoodsRequest
	3, // 2: goods.Goods.CrtGoods:input_type -> goods.CrtRequest
	2, // 3: goods.Goods.CreateGoods:output_type -> goods.GoodsResponse
	4, // 4: goods.Goods.CrtGoods:output_type -> goods.CrtResponse
	3, // [3:5] is the sub-list for method output_type
	1, // [1:3] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_goods_proto_init() }
func file_goods_proto_init() {
	if File_goods_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_goods_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GoodsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_goods_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Good); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_goods_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GoodsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_goods_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CrtRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_goods_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CrtResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_goods_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_goods_proto_goTypes,
		DependencyIndexes: file_goods_proto_depIdxs,
		MessageInfos:      file_goods_proto_msgTypes,
	}.Build()
	File_goods_proto = out.File
	file_goods_proto_rawDesc = nil
	file_goods_proto_goTypes = nil
	file_goods_proto_depIdxs = nil
}
