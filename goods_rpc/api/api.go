package api

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"rpc/global"
	pb "rpc/goods"
	"rpc/models"
	"strconv"
	"time"
)

type Server struct {
	pb.UnimplementedGoodsServer
}

func (s *Server) CreateGoods(ctx context.Context, in *pb.GoodsRequest) (*pb.GoodsResponse, error) {

	g := models.Goods{
		GoodsName:  in.GoodsName,
		GoodsPrice: int64(in.GoodsPrice),
		GoodsCount: int64(int(in.GoodsCount)),
		GoodsTest:  in.GoodsTest,
	}

	err := global.Db.Create(&g).Error
	if err != nil {
		return nil, errors.New("商品添加失败")
	}

	good := &pb.Good{
		Id:         int64(g.ID),
		GoodsName:  g.GoodsName,
		GoodsPrice: float32(g.GoodsPrice),
		GoodsCount: int64(g.GoodsCount),
		GoodsTest:  g.GoodsTest,
	}

	return &pb.GoodsResponse{G: good}, nil
}

func (s *Server) CrtGoods(ctx context.Context, in *pb.CrtRequest) (*pb.CrtResponse, error) {
	var g models.Goods
	uuidNew := uuid.NewString()
	result, err := global.Redis.SetNX(context.Background(), global.RedisKey, uuidNew, time.Second*5).Result()
	if err != nil {
		return nil, errors.New("锁设置失败")
	}

	//添加事务
	etx := global.Db.Begin()
	err = etx.Where("id", in.GoodsId).First(&g).Error
	if err != nil {
		return nil, errors.New("数据库查询失败")
	}
	//判断库存是否充足
	if in.GoodsNumber > g.GoodsCount {
		return nil, errors.New("库存不足")
	}

	price := in.GoodsNumber * g.GoodsPrice
	zon := price - in.GoodsYou
	order := models.Order{
		OrderUid:   uuidNew,
		GoodsId:    int(in.GoodsId),
		GoodsCount: in.GoodsNumber,
		GoodsPrice: float64(price),
		GoodsYou:   float64(in.GoodsYou),
		GoodsZon:   float64(zon),
	}
	if result {

		b, err := global.Redis.Get(context.Background(), global.RedisKey).Result()
		if err != nil && b != uuidNew {
			fmt.Println("锁获取错误")
		}
		err = global.Db.Create(&order).Error
		if err != nil {
			return nil, errors.New("订单生成失败")
		}
		//扣减库存
		g.GoodsCount -= in.GoodsNumber
		err = etx.Save(&g).Error
		if err != nil {
			return nil, errors.New("库存更新失败")
			etx.Rollback()
		}
		etx.Commit()
		global.Redis.Del(context.Background(), global.RedisKey)
		fmt.Println("锁删除成功")

	} else {
		return nil, errors.New("锁获取失败")

	}

	return &pb.CrtResponse{
		OrderUid:   uuidNew,
		GoodsPrice: strconv.FormatInt(zon, 10),
	}, nil

}
