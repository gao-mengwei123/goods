package models

import "gorm.io/gorm"

//用户表

type User struct {
	gorm.Model
	Name     string
	Mobile   string
	Password string
}

// 商品表
type Goods struct {
	gorm.Model
	GoodsName  string
	GoodsPrice int64
	GoodsCount int64
	GoodsTest  string
	GoodsId    int
}

// 订单表
type Order struct {
	gorm.Model
	OrderUid   string  //订单编号
	GoodsId    int     //商品ID
	GoodsCount int64   //数量
	GoodsPrice float64 //总价格
	GoodsYou   float64 //优惠价
	GoodsZon   float64 //商品最后的价格
	Good       []Goods `gorm:"foreignKey:GoodsId"`
}
